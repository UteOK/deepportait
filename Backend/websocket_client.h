/*
 *  ██████╗ ███████╗███████╗██████╗     ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
 *  ██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
 *  ██║  ██║█████╗  █████╗  ██████╔╝    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
 *  ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
 *  ██████╔╝███████╗███████╗██║         ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
 *  ╚═════╝ ╚══════╝╚══════╝╚═╝         ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝
 *
 * VERSION 1.0
 *
 * HDM Stuttgart
 * Medieninformatik
 * Semester Projekt 2018/2019
 *
 * Authorin :  Ute Orner-Klaiber
 *
 * Datum    :  4.2.2019
 *
 * Betreuer :  Benjamin Binder, Patrick Bader
 *
*/

#ifndef WEBSOCKET_CLIENT_H
#define WEBSOCKET_CLIENT_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocket>
#include <QPixmap>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class websocket_client : public QObject
{
    Q_OBJECT
public:
    explicit websocket_client(QObject *parent = nullptr);

Q_SIGNALS:
    void closed();


signals:
    void next_request();
    void req_queue_finished();

public slots:
    void next_request_sl();
    void first_request();
    void next_response(QByteArray recv_data);


public:
    void set_url(const QUrl &url);
    void run();
    void add_data(std::string cmd);
    void add_data(QImage &image);
    void add_data(Mat &image);
    void clear_queues();

    QImage get_data();


private:
    QWebSocket ws_webSocket;
    QUrl ws_url;
    QImage mat_to_qimage_cpy(cv::Mat const &mat, QImage::Format format);
    QByteArray qimage_to_compressed_image(QImage image);
    QByteArray mat_to_compressed_image(Mat image);
    QImage compressed_image_to_qimage(QByteArray image_array);

    std::vector<QByteArray> requ_queue;
    std::vector<QByteArray> resp_queue;


};

#endif // WEBSOCKET_CLIENT_H
