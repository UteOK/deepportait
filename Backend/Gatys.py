import sys
sys.path.append("../../DeepStyle/")
import DeepStyleGatys as gatys


print("Starte Style Transfer")

gatys.styletransfer("content_image.jpg",
					"style_image.jpg",
					1000,
					1,10**3,
					1.5,
					['conv4_2'],
					['conv1_1','conv2_1','conv3_1','conv4_1','conv5_1'],
					[0.1,0.1,0.1,0.1,0.6],
					save_generated="generated_image.jpg"
				  )

