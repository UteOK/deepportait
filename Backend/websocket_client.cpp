/**
 *  ██████╗ ███████╗███████╗██████╗     ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
 *  ██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
 *  ██║  ██║█████╗  █████╗  ██████╔╝    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
 *  ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
 *  ██████╔╝███████╗███████╗██║         ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
 *  ╚═════╝ ╚══════╝╚══════╝╚═╝         ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝
 *
 * VERSION 1.0
 *
 *
 * HDM Stuttgart
 * Medieninformatik
 * Semester Projekt 2018/2019
 *
 * Authorin :  Ute Orner-Klaiber
 * Datum    :  4.2.2019
 * Betreuer :  Benjamin Binder, Patrick Bader
 *
 *
 * @file     websocket_client.cpp
 * @author   Ute Orner-Klaiber
 * @brief    Die Klasse websocket_client stellt eine Verbindung zu einer Instanz der Klasse websoket_server
 *           her, transferiert Content Bild und Style Bild, startet Style Transfer und empfaengt das Generated Bild.
 *
 *
*/

#include "websocket_client.h"
#include <QtCore/QDebug>
#include <QTimer>
#include <QEventLoop>

#include <opencv2/imgcodecs.hpp>
using namespace std;
using namespace cv;

QT_USE_NAMESPACE


/** websocket_client Constructor
*
* @param url URL des Websocket Servers
* @param parent QT Parent
*/


websocket_client::websocket_client(QObject *parent) :
    QObject(parent)
{}


void websocket_client::set_url(const QUrl &url){
    this->ws_url = url;
}

/** Stellt die Verbindung zum Websocket Server her.
*
*/
void websocket_client::run(){
    connect(&ws_webSocket, &QWebSocket::connected,           this, &websocket_client::first_request);
    connect(&ws_webSocket, &QWebSocket::binaryFrameReceived, this, &websocket_client::next_response);
    connect(this,          &websocket_client::next_request,  this, &websocket_client::next_request_sl);

    qDebug() << "size of request  Queue " <<this->requ_queue.size();
    qDebug() << "size of response Queue " <<this->resp_queue.size();
    ws_webSocket.open(QUrl(this->ws_url));
}

/** Löst nur den next_request() aus, dient zur Debugausgabe.
*
*/
void websocket_client::first_request(){
    qDebug() << "erster Request ";
    next_request();
}

/** Wird von next_request() getriggert. Wenn die Request-Queue nicht leer ist, wird das erste Element in einem QByteArray gespeichert.
 * Anschließend wird dieses Element aus der Queue gelöscht. Und mit sendBinaryMessage() wird das Element an den Server geschickt.
*
*/

void websocket_client::next_request_sl(){
    qDebug() << "nextRequest reqQ" << requ_queue.size() << " respQ" << resp_queue.size();;

    if(!this->requ_queue.empty()){ //nicht empty
        QByteArray req = this->requ_queue[0]; //nimmt erstes Element
        requ_queue.erase(requ_queue.begin()); //löscht dieses Element
        ws_webSocket.sendBinaryMessage(req); //sendet dieses Element an Jarvis
        qDebug() << "sent request " << req;
    }
}

/** Verarbeitet die Message (msg) vom Server und added die Message in die Response_Queue.
 * Außerdem wird geprüft, ob die Request_Queue leer ist, wenn sie nicht leer ist, wird der nächste Request aufgerufen.
** @param msg QByteArray, kommt von Server
*/
void websocket_client::next_response(QByteArray msg){
    this->resp_queue.push_back(msg);
    if (msg.size()< 100)
        qDebug() << "received message " << msg;
    else
        qDebug() << "received message of length " << msg.size();


    if(this->requ_queue.empty()){
        for(int i=0; i < this->resp_queue.size();i++){
            qDebug() << i << " " << this->resp_queue[i].size();
        }

        qDebug() << "Emit req_queue_finished";
        emit req_queue_finished();

    }
    else{
        emit next_request();
    }
}

/** String wird in QByteArray umgewandelt (weil so werden Daten an Server geschickt) und in Request_Queue geschrieben.
**
* @param cmd String, der an WebSocket Server gesendet werden soll
*/
void websocket_client::add_data(std::string cmd){
    requ_queue.push_back(QByteArray::fromStdString(cmd));
}

/** QImage wird als jpg-komprimierten QByteArray gespeichert und in Request_Queue geschrieben.
**
* @param image QImage, das an WebSocket Server gesendet werden soll
*/
void websocket_client::add_data(QImage &image){
    QByteArray qa = qimage_to_compressed_image(image);
    requ_queue.push_back(qa);
}

/** Mat wird als jpg-komprimierten QByteArray gespeichert und in Request_Queue geschrieben.
**
* @param image Mat, das an WebSocket Server gesendet werden soll
*/
void websocket_client::add_data(Mat &image){
    QByteArray qa = mat_to_compressed_image(image);
    requ_queue.push_back(qa);
}


/** Liest die letzte Antwort des Websocket Servers und wandelt das komprimierte QByteArray in ein QImage um.
**
* @return QImage mit dem letzten Bild, das der Websocket Server gesendet hat
*/
QImage websocket_client::get_data(){
    QImage img;
    int position = resp_queue.size()-1; //so bekommt man den Index vom letzten Element, Generated Image ist immer letztes Element
    qDebug() << "getData:: respQ size: " << resp_queue.size();
     if (resp_queue.size() > position){
        QByteArray msg = resp_queue[position];
        img     = compressed_image_to_qimage(msg);
     }
    else{
        qDebug() << "Error: Listen position " << position << " nicht gefunden";
     }
    return img;
}

/** Leert beide Queues.
**
*/
void websocket_client::clear_queues(){
    resp_queue.clear();
    requ_queue.clear();
}


/** Konversion von QByteArray zu QImage
**
* @param image_array QByteArray
* @return dekomprimiertes QImage
*/
QImage websocket_client::compressed_image_to_qimage(QByteArray image_array ){
    qDebug() << "compressed_image_to_qimage";
    qDebug() << " ByteArray received of size " << image_array.size() ;
    std::string image_as_string = image_array.toStdString();
    std::vector<uchar> buffer;
    std::copy( image_as_string.begin(), image_as_string.end(), std::back_inserter(buffer));
    cv::Mat image_as_mat = cv::imdecode( buffer, IMREAD_COLOR);

    cvtColor(image_as_mat, image_as_mat, COLOR_BGR2RGB);

    QImage image_as_qimage( image_as_mat.data,image_as_mat.cols, image_as_mat.rows,
                  static_cast<int>(image_as_mat.step),QImage::Format_RGB888 );

    image_as_qimage = image_as_qimage.rgbSwapped();

    return image_as_qimage;
}

/** Konversion von QImage zu QByteArray
**
* @param image_as_qimage QImage
* @return QByteArray, das jpg-komprimiertes Bild enthaelt
*/
QByteArray websocket_client::qimage_to_compressed_image(QImage image_as_qimage){
    image_as_qimage = image_as_qimage.rgbSwapped();

    qDebug() <<"QImage size " << image_as_qimage.sizeInBytes();
    cv::Mat image_as_mat(image_as_qimage.height(), image_as_qimage.width(),
                         CV_8UC4,const_cast<uchar*>(image_as_qimage.bits()),
                     static_cast<size_t>(image_as_qimage.bytesPerLine()));


    std::vector<uchar> buffer;
    buffer.resize(10);
    std::vector<int> params;
    params.push_back(cv::IMWRITE_JPEG_QUALITY);
    params.push_back(95);
    cv::imencode(".jpg", image_as_mat, buffer, params);

    std::string image_as_string(buffer.begin(), buffer.end());
    QByteArray image_as_qbytearray = QByteArray::fromStdString(image_as_string);

    return image_as_qbytearray;
}

/** Konversion von OpenCV::Mat zu QByteArray
**
* @param image_as_mat OpenCV::Mat Bild
* @return QByteArray, weil so muss es an Server geschickt werden
*/
QByteArray websocket_client::mat_to_compressed_image(Mat image_as_mat){

    std::vector<uchar> buffer;
    buffer.resize(10);
    std::vector<int> params;
    params.push_back(cv::IMWRITE_JPEG_QUALITY);
    params.push_back(95);
    cv::imencode(".jpg", image_as_mat, buffer, params);


    std::string image_as_string(buffer.begin(), buffer.end());
    QByteArray image_as_qbytearray = QByteArray::fromStdString(image_as_string);

    return image_as_qbytearray;
}
