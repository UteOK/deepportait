#!/usr/bin/env python
import asyncio
import websockets
import numpy as np
import time
import logging
import threading
import cv2
import random
import subprocess
from PIL import Image
from tensorflow.python.keras.preprocessing import image as K_image
from shutil import copyfile

##################################################################
#Konfiguration

# Python Executable aus Virtual Environment das alle Pakete für Gatys.py installiert hat
py_executable = r'C:\Users\Ute\AppData\Local\Continuum\anaconda3\envs\deepstyle\python.exe'
# IP Adresse des Network Interfaces, das verwendet werden soll
ip_address    = "192.168.1.197"
# Port für Websocket
port          = 1133

##################################################################


# Content und Style Image werden auf Server als jpg gespeichert
def run2():
	global content_image, style_image
	
	logger.info("compressed content image")
	
	ci_compressed = np.frombuffer(content_image, np.ubyte)
	ci_array = cv2.imdecode(ci_compressed, cv2.IMREAD_COLOR) #in ci_array ist jetzt das Content Image gespeichert 
	logger.info("Uncompressed image size : " + str(ci_array.shape))
	
	si_compressed = np.frombuffer(style_image, np.ubyte)
	si_array = cv2.imdecode(si_compressed, cv2.IMREAD_COLOR) #in si_array ist jetzt das Style Image gespeichert 
	
	logger.info("Uncompressed style image size : " + str(si_array.shape))
	logger.info(ci_array)
	logger.info(si_array)

	
	ci_image = Image.fromarray(ci_array, mode="RGB")
	ci_image.save("content_image.jpg") #Content Image wird als "content_image.jpg" auf Server gespeichert, darauf kann dann der Style Transfer Code zugreifen
	
	si_image = Image.fromarray(si_array, mode="RGB")
	si_image.save("style_image.jpg") #Style Image wird auch auf Server abgelegt
	
	subprocess.Popen([py_executable,'Gatys.py']) # Paralle Ausführung, weil die Generated Images (jedes 30igste) werden weggeschickt, 
	                                                                                # damit der User den Style Transfer life sehen kann

#Generated Image wird so bearbeitet, dass es an Client geschickt werden kann
def get_generated_image():
	image = Image.open("generated_image.jpg")
	image = image.convert(mode="RGB")

	image = K_image.img_to_array(image)	
	return cv2.imencode(".jpg",image)[1].tobytes()

	
async def command_fsm(websocket, path):
	global content_image, style_image, generated_image
	
	cmd = ""
	
	while True:
		logger.info("@@@ WAITING for command")
		try:
			cmd = await websocket.recv()
		except websockets.ConnectionClosed:	
			logger.info("Client closed connection")
		print("Received command " + str(cmd))
		
		if (cmd == b"echo"):
			await websocket.send(b"READY")
			logger.info("ECHO Message")
		
		elif(cmd == b"content"): # diese Strings kommen vom Client
			copyfile("../Frontend/Bilder/gpu.jpg", "generated_image.jpg") #gpu Bild ist jetzt Generated Image,weil das soll am Anfang angezeigt werden, bis es "echte" Generated Images gibt
			await websocket.send(b"OK: CMD content")
			logger.info("Waiting for content image")
			content_image = await websocket.recv()	#Content Image kommt von Client und ist jetzt in content_image gespeichert
			await websocket.send(b"OK: DATA content") 
			logger.info("Received content image")
		elif(cmd == b"style"):
			await websocket.send(b"OK: CMD style")
			logger.info("Waiting for style image")
			style_image = await websocket.recv()  #Style Image kommt von Client und ist jetzt in style_image gespeichert
			await websocket.send(b"OK: DATA style")
			logger.info("Received style image")
		elif(cmd == b"run"):
			await websocket.send(b"OK")
			print("Running style transfer")
			run2() #run2 Methode wird aufgerufen
			logger.info("ST running in background")
		elif(cmd == b"generated"): #dieser Befehl bewirkt, dass Generated Image an Client gesendet wird
			logger.info("Returning generated image")
			try:
				generated_image = get_generated_image() 
			except:
				print("problem beim lesen")
			await websocket.send(generated_image)

			logger.info("Done Returning generated image")
		else:
			await websocket.send(b"ERR")
			logger.info("Command " + str(cmd) + " unknown. Resetting.")
			content_image    = ""
			style_image      = ""
			generated_image  = ""
			
		logger.info("content image: " + str(len(content_image)))
		logger.info("style image:   " + str(len(style_image)))



#Globale Variablen	
content_image    = ""
style_image      = ""
generated_image  = ""
iter             = "0"

print("██████╗ ███████╗███████╗██████╗     ") 
print("██╔══██╗██╔════╝██╔════╝██╔══██╗    ") 
print("██║  ██║█████╗  █████╗  ██████╔╝    ") 
print("██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ")
print("██████╔╝███████╗███████╗██║         ")
print("╚═════╝ ╚══════╝╚══════╝╚═╝         ")
print("")
print("██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗")
print("██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝")
print("██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║")
print("██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║")
print("██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║")
print("╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝")
print("Server is running")

#Logger
logger = logging.getLogger('websocket_sever')
fh = logging.FileHandler('style_transfer_websocket_server.log')
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

#Server starten
start_server = websockets.serve(command_fsm, ip_address, port) #Server läuft auf Port 1133
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
logger.info("Sever gestartet")



	
	
