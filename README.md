[Watch Demo on YouTube](https://www.youtube.com/watch?v=wOtbknmv1SA)

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/wOtbknmv1SA/0.jpg)](https://www.youtube.com/watch?v=wOtbknmv1SA)


    ██████╗ ███████╗███████╗██████╗    
    ██╔══██╗██╔════╝██╔════╝██╔══██╗    
    ██║  ██║█████╗  █████╗  ██████╔╝    
    ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     
    ██████╔╝███████╗███████╗██║         
    ╚═════╝ ╚══════╝╚══════╝╚═╝         

    ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
    ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
    ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
    ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝

VERSION 2.0


HDM Stuttgart

Medieninformatik

Präsentation auf GameZone des Internationalen Trickfilmfestivals 2019


Authorin       :  Ute Orner-Klaiber

Betreuerin     :  Sabiha Ghellal