/**
 *  ██████╗ ███████╗███████╗██████╗     ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
 *  ██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
 *  ██║  ██║█████╗  █████╗  ██████╔╝    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
 *  ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
 *  ██████╔╝███████╗███████╗██║         ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
 *  ╚═════╝ ╚══════╝╚══════╝╚═╝         ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝
 *
 * VERSION 1.0
 *
 *
 * HDM Stuttgart
 * Medieninformatik
 * Semester Projekt 2018/2019
 *
 * Authorin :  Ute Orner-Klaiber
 * Datum    :  25.02.2019
 * Betreuer :  Benjamin Binder, Patrick Bader
 *
 *
 * @file     deepportrait.cpp
 * @author   Ute Orner-Klaiber
 *
 *
 *
*/

#include "deepportrait.h"
#include "ui_deepportrait.h"
#include <QPixmap>
#include <opencv2/imgproc.hpp>
#include <QDirIterator>
#include <QDebug>
#include <QBoxLayout>
#include "Backend/websocket_client.h"


using namespace std;
using namespace cv;


/** DeepPortrait Konstruktor liest Parameter ein, startet Kamera und stellt Websocket Verbindung zum Server her.
**
*/
DeepPortrait::DeepPortrait(QWidget *parent) :
    QMainWindow(parent),
    timer(this),
    timerprogress(this),
    ui(new Ui::DeepPortrait),

    client()

{

    //Parameter einlesen und Verbindung zum Server werden erst gestartet, nachdem UI gestartet ist.
    //Ansonsten werden Fehlermeldungen nicht angezeigt.
    QTimer::singleShot(3000, this, SLOT(read_settings_and_connect_websocket()) );
    ui->setupUi(this);
    cam.open(0);
    connect(&timer, SIGNAL(timeout()), this, SLOT(fotostream())); //im fotostream wird Video im camview_Label angezeigt
    timer.start(30); //alle 30ms ein Foto

    ui->stackedWidget->setCurrentIndex(STARTPAGE_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background1.png) 0 0 0 0 stretch stretch;"
                "}");


    connect(&timerprogress, SIGNAL(timeout()), this, SLOT(update_progress_bar()));

    connect(ui->home_button_2, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_3, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_4, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_5, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_6, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_7, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);
    connect(ui->home_button_8, &QPushButton::clicked, this, &DeepPortrait::on_home_button_clicked);


    connect(ui->exit_button_1, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_2, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_3, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_4, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_5, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_6, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_7, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_8, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect(ui->exit_button_9, SIGNAL( clicked() ), this, SLOT( close() ) );

}
//Ende Konstruktor

DeepPortrait::~DeepPortrait()
{
    delete ui;
}


/** User klickt start_Button und gelangt auf die Seite, wo man den make_your_own_Button ausführen kann.
**
*/
void DeepPortrait::on_start_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(MAKE_YOUR_OWN_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background2.png) 0 0 0 0 stretch stretch;"
                "}");
}

/** User klickt make_your_own_Button und gelangt auf die Seite wo man ein Foto von sich machen kann.
**
*/
void DeepPortrait::on_make_your_own_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(MAKE_A_FOTO_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/fotomachen.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->take_a_photo_button->setEnabled(true);
    ui->use_artificial_intelligence_button->setEnabled(true);



}

/** Das Foto/Video wird von der Kamera aufgenommen, skaliert und wird im camview_Label angezeigt.
* OpenCV nimmt Fotos/Videos mit dem Farbprofil BGR auf, deshalb muss es nach RGB konvertiert werden.
*/
void DeepPortrait::fotostream()
{
    Mat foto_von_camera;
    QImage foto_as_QImage;
    QPixmap foto_as_QPixmap; //damit es in Label angezeigt werden kann

    cam >> foto_von_camera;
    flip(foto_von_camera, foto_von_camera, +1);
    cvtColor(foto_von_camera, foto_von_camera, COLOR_BGR2RGB);
    foto_as_QImage = QImage((uchar*) foto_von_camera.data, foto_von_camera.cols, foto_von_camera.rows, foto_von_camera.step, QImage::Format_RGB888);

    foto_as_QPixmap = scale_image_and_show_in_label(foto_as_QImage, 381);
    ui->camview_label->setPixmap(foto_as_QPixmap);
}


/** User nimmt ein Foto von sich auf. Das Foto der Kamera wird als Content Image ausgewählt.
* Das aufgenommene Foto wird beschnitten (quadratisch), weil der Style Transfer ein quadratisches Foto erwartet.
* Das Foto wird im show_foto_Label angezeigt.
*/
void DeepPortrait::on_take_a_photo_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(SHOW_FOTO_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/fotoanzeigen4.png) 0 0 0 0 stretch stretch;"
                "}");

    Mat foto_von_camera;

    cam >> foto_von_camera; //Hier wird das Foto gemacht.

    flip(foto_von_camera, foto_von_camera, +1);
    cvtColor(foto_von_camera, foto_von_camera, COLOR_BGR2RGB);
    content_image_mat = foto_von_camera.clone ();
    int h = content_image_mat.rows;
    int w = content_image_mat.cols;
    int newh = min(h,w);
    int neww = min(h,w);
    Rect quadratcrop(0, 0, newh, neww);
    content_image_mat = content_image_mat(quadratcrop);

    QImage foto_as_QImage = QImage((uchar*) foto_von_camera.data, foto_von_camera.cols, foto_von_camera.rows, foto_von_camera.step, QImage::Format_RGB888); //Format_RGB32
    QPixmap foto_as_QPixmap = scale_image_and_show_in_label(foto_as_QImage, 381);
    ui->show_foto_label->setPixmap(foto_as_QPixmap);

    content_image = foto_as_QImage.copy();

    ui->take_a_photo_button->setEnabled(false);
    ui->choose_style_button->setEnabled(true);
}

/** User entscheidet sich, ein neues Foto von sich aufzunehmen und geht ein Schritt zurück.
**
*/
void DeepPortrait::on_oh_ugly_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(MAKE_A_FOTO_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/fotomachen.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->take_a_photo_button->setEnabled(true);
}

/** User kann hier das Style Image auswählen. Content Image wird in show_foto_Label_2 angezeigt.
 * Das Verzeichnis, das im JSON file als Style_directory gesetzt wird, wird nach JPG Bildern durchsucht.
 * Die gefundenen JPG Bilder werden in der unteren Leiste (Scrollbar) alle angezeigt.
**
*/
void DeepPortrait::on_choose_style_button_clicked()
{

    ui->stackedWidget->setCurrentIndex(CHOOSE_STYLE_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background5.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->use_style_button->setVisible(false);
    ui->take_a_photo_button->setEnabled(true);
    ui->choose_style_button->setEnabled(false);

    QPixmap foto_as_QPixmap = scale_image_and_show_in_label(content_image, 200);
    ui->show_foto_label_2->setPixmap(foto_as_QPixmap);


    //lädt die Stylebilder in Scrollbar:
    signalMapper = new QSignalMapper(this);
    QHBoxLayout *styleimg = new QHBoxLayout(this);

    QDirIterator it(this->settings["Style_directory"], QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QString iter = it.next();
        if (iter.endsWith("jpg")||iter.endsWith("jpeg")||iter.endsWith("JPG")){
            QImage style(iter);
            float width = style.width();
            float height = style.height();
            float ratio = width/height;
            int nheight = 180;
            int nwidth = ratio*180;

            QPushButton *pb = new QPushButton();
            QPixmap pixmap(QPixmap::fromImage(style.scaled(nwidth,nheight)));
            QIcon ButtonIcon(pixmap);
            pb->setIcon(ButtonIcon);
            pb->setIconSize(pixmap.rect().size());
            styleimg->addWidget(pb);

            QObject::connect(pb, SIGNAL(clicked()), signalMapper, SLOT(map()));
            signalMapper->setMapping(pb, iter);
        }
    }
    connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(show_in_label(QString)));
    ui->scrollstyle->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    ui->scrollcontent->setLayout(styleimg);
}
/** Bild mit Datei "iter" (darin ist Name der Datei enthalten) wird als Style Image ausgewählt und angezeigt.
**
* @param iter String zu JPG file
*/
void DeepPortrait::show_in_label(QString iter){
    ui->use_style_button->setVisible(true);
    QPixmap stylebild(iter);
    QImage temp(iter);

    style_image = temp.copy();
    style_image = style_image.convertToFormat(QImage::Format_RGB32);
    ui->show_style_big_label->setPixmap(stylebild.scaled(300,300));

}

/** User bestätigt Auswahl des Style Images. Content Image und Style Image werden angezeigt.
**
*/
void DeepPortrait::on_use_style_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(USE_AI_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background6.png) 0 0 0 0 stretch stretch;"
                "}");

    QPixmap content_as_QPixmap = scale_image_and_show_in_label(content_image, 400);
    ui->content_image_label->setPixmap(content_as_QPixmap);

    QPixmap style_as_QPixmap = scale_image_and_show_in_label(style_image, 400);
    ui->style_image_label->setPixmap(style_as_QPixmap);

    ui->show_style_right_label->clear();
    ui->show_content_left_label->clear();
    ui->use_artificial_intelligence_button->setEnabled(true);

}


/** User klickt den use_artificial_intelligence_Button, dies führt zum ersten Mal send_content_style aus und stellt die connect Statements her, welche kontinuierlich "Generated Images" vom Server anfordern.
**
*/
void DeepPortrait::on_use_artificial_intelligence_button_clicked()
{
    connect(&client, &websocket_client::req_queue_finished,  this, &DeepPortrait::on_manage_generated_image_button_clicked);
    connect(this, &DeepPortrait::next_generated_image,  this, &DeepPortrait::send_generated_image_request);

    send_content_style();

    ui->use_artificial_intelligence_button->setEnabled(false);
    ui->home_button_7->setEnabled(false);
    ui->exit_button_7->setEnabled(false);

    // damit porgress bar richtig angezeigt wird
    int update = 60;
    timerprogress.start(update * (1000/100));
    progress = 0;

    if (progress<100){
        ui->style_transfer_progress_bar->show();
        ui->send_email_button->hide();
        ui->back_to_beginning_button->hide();
    }
}

/** Loop zum Empfang von Generated Images wird unterbrochen
**
*/
void DeepPortrait::on_reset_generated_image_request()
{
    disconnect(this, &DeepPortrait::next_generated_image,  this, &DeepPortrait::send_generated_image_request);
    disconnect(&client, &websocket_client::req_queue_finished,  this, &DeepPortrait::on_manage_generated_image_button_clicked);
}

/** Request zum Senden des Generated Image wird an den Server geschickt.
* Server bekommt den Befehl "generated" als String und der Befehl wird auf dem Server "bearbeitet".
* Das Generated Image wird nicht nur einmal, sondern alle 30 Iterationen des Style Transfers werden angezeigt.
* Dadurch entsteht das "Video" und der User kann life den Fortschritt betrachten.
* Es werden bewusst nur alle 30 Iterationen angezeigt, weil ansonsten sich das Auge zu sehr an das Bild gewöhnt und
* man erkennt gar keinen Fortschritt mehr. Durch die 30 Iterationen "springt" das Bild und man erkennt besser den Fortschritt.
*/
void DeepPortrait::send_generated_image_request(){
    client.add_data("generated");
    client.first_request();
}

/** Content Image, Style Image und der erste Request zum Erstellen eines Generated Image werden an den Server gesendet.
* Das ist das Protokoll, der String ist der Befehl an den Server, je nachdem welcher String kommt, macht der Server etwas anderes.
*/
void DeepPortrait::send_content_style(){

    client.add_data("content");
    client.add_data(content_image_mat); //nicht Qimage geadded sondern OpenCV image (mat)
    client.add_data("style");
    client.add_data(style_image); //QImage wird geadded
    client.add_data("run");
    client.add_data("generated");
    client.first_request();
}


/** Empfangene Generated Images werden in der Mitte angezeigt, links davon das Foto (Content Image), rechts das Style Image.
 * Der Request zum Empfangen des nächsten Generated Image wird getriggert.
** Dieser "Button" wird in der GUI nicht angezeigt, er löst nur ein gewünschtes Event aus.
** Wird ausgelöst wenn man auf on_use_artificial_intelligence_Button geklickt hat.
*/
void DeepPortrait::on_manage_generated_image_button_clicked(){
    qDebug() << "on_manage_generated_image_button_clicked";
    QImage generated_image_as_QImage = client.get_data(); //Weil "generated" der letzte Befehl ist
    generated_image = generated_image_as_QImage.copy();
    client.clear_queues(); //Alle queues in client leeren: dann ist beim nächsten Mal das Generated Image wieder an der gewünschten Position

    ui->stackedWidget->setCurrentIndex(SHOW_PROGRESS_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background5.png) 0 0 0 0 stretch stretch;"
                "}");

    QPixmap generated_image_as_QPixmap = scale_image_and_show_in_label(generated_image_as_QImage, 350);
    ui->show_generated_image_label->setPixmap(generated_image_as_QPixmap);

    QPixmap content_left = scale_image_and_show_in_label(content_image, 200);
    ui->show_content_left_label->setPixmap(content_left);

    QPixmap style_right = scale_image_and_show_in_label(style_image, 200);
    ui->show_style_right_label->setPixmap(style_right);

    emit next_generated_image();
}

/** User möchte Bild nicht per E-Mail zugesendet bekommen. Geht zurück an Anfang.
**
*/
void DeepPortrait::on_back_to_beginning_button_clicked()
{
    on_reset_generated_image_request();

    ui->stackedWidget->setCurrentIndex(MAKE_YOUR_OWN_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background2.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->show_style_big_label->clear();
    ui->show_generated_image_label->clear();
    ui->home_button_7->setEnabled(true);
    ui->exit_button_7->setEnabled(true);


}
/** User möchte Bilder per E-Mail zugeschickt bekommen. Email-Adresse ist aber noch nicht eingegeben.
**
*/
void DeepPortrait::on_send_email_button_clicked()
{
    on_reset_generated_image_request();

    ui->stackedWidget->setCurrentIndex(SEND_EMAIL_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/backgroundemail7.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->send_finaly_email_button->setEnabled(true);
    ui->home_button_7->setEnabled(true);
    ui->exit_button_7->setEnabled(true);
    ui->valid_email_label->hide();
}

/** Send Button geklickt --> Style, Content und Generated Image werden mit Hilfe der smtp Klasse an SMTP Server geschickt.
** Email-Adresse ist richtig eingegeben. Es wird geprüft, ob die EMail-Adresse dem richtigen Pattern entspricht.
*/
void DeepPortrait::on_send_finaly_email_button_clicked()
{

    QRegExp mailREX("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    mailREX.setCaseSensitivity(Qt::CaseInsensitive);
    mailREX.setPatternSyntax(QRegExp::RegExp);
    QString empfaenger = ui->email_text_edit->toPlainText();


    if (mailREX.exactMatch(empfaenger)==false){
        ui->valid_email_label->show();
    }
    else {

        ui->stackedWidget->setCurrentIndex(THANK_YOU_INDEX);
        this->setStyleSheet(
                    "#centralWidget { "
                    " border-image: url(:/Images/Frontend/Bilder/background8.png) 0 0 0 0 stretch stretch;"
                    "}");

        style_image = style_image.scaled(400, 400,Qt::KeepAspectRatio);
        content_image = QImage((uchar*) content_image_mat.data, content_image_mat.cols, content_image_mat.rows, content_image_mat.step, QImage::Format_RGB888);
        content_image = content_image.scaled(400, 400,Qt::KeepAspectRatio);
        generated_image.save("FancyDeepPortrait.jpg",0,90);
        style_image.save("StyleImage.jpg",0,90);
        content_image.save("You.jpg",0,90);

        Smtp* smtp = new Smtp(this->settings["Smtp_username"],
                              this->settings["Smtp_password"],
                              this->settings["Smtp_server"],
                              this->settings["Smtp_port"].toInt());


        QString emailtext = "Thank you for visiting the opening of the Institute for Applied AI at HdM Stuttgart and using my artificial intelligence made by my natural intelligence :-) .\n\n\nUte Orner-Klaiber \nlinkedin.com/in/uteOK ";
        QString betreff = "Your DeepPortrait from the opening of the Institute for Applied AI at HdM Stuttgart";
        QStringList portrait;
        portrait.append("FancyDeepPortrait.jpg");
        portrait.append("StyleImage.jpg");
        portrait.append("You.jpg");
        smtp->sendMail("deepportraithdm@gmail.com", empfaenger , betreff, emailtext , portrait );

        ui->send_finaly_email_button->setEnabled(false);
        ui->valid_email_label->hide();

        //delete smtp;
    }

}
/** Zurück zum Start, nachdem E-Mail versendet wurde.
**
*/
void DeepPortrait::on_back_to_start_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(MAKE_YOUR_OWN_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background2.png) 0 0 0 0 stretch stretch;"
                "}");
    ui->email_text_edit->clear();
    ui->show_style_big_label->clear();
    ui->show_generated_image_label->clear();
    ui->valid_email_label->hide();
}

/** User möchte wieder ein Schritt zurück in der GUI.
**
*/
void DeepPortrait::on_one_step_back_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(SHOW_PROGRESS_INDEX);
    QPixmap b7(":/Images/Frontend/Bilder/background5.png");
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background5.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->send_finaly_email_button->setEnabled(true);
    ui->valid_email_label->hide();
    ui->email_text_edit->clear();

}

/** Wechsel zu Startseite wenn Home Button im linken oberen Eck gecklickt wird.
**
*/
void DeepPortrait::on_home_button_clicked()
{
    on_reset_generated_image_request();
    QPixmap b2(":/Images/Frontend/Bilder/background2.png");
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/background2.png) 0 0 0 0 stretch stretch;"
                "}");
    ui->stackedWidget->setCurrentIndex(MAKE_YOUR_OWN_INDEX);
    ui->email_text_edit->clear();
    ui->show_style_big_label->clear();
    ui->show_generated_image_label->clear();
    ui->valid_email_label->hide();
    progress = 0;


}


/** User möchte nochmal eine EMail-Adresse eingeben, geht ein Schritt zurück in der GUI.
**
*/
void DeepPortrait::on_back_to_enter_email_button_clicked()
{
    ui->stackedWidget->setCurrentIndex(SEND_EMAIL_INDEX);
    this->setStyleSheet(
                "#centralWidget { "
                " border-image: url(:/Images/Frontend/Bilder/backgroundemail7.png) 0 0 0 0 stretch stretch;"
                "}");

    ui->email_text_edit->clear();
    ui->valid_email_label->hide();
    ui->send_finaly_email_button->setEnabled(true);

}


/** Update der Progress-Bar, während das Generated Image vom Server empfangen wird.
**
*/
void DeepPortrait::update_progress_bar(){
    ui->style_transfer_progress_bar->setValue(progress);
    if (progress<100){
        progress +=1;
    }
    if (progress==100) {
        ui->send_email_button->show();
        ui->back_to_beginning_button->show();
        ui->style_transfer_progress_bar->hide();
        ui->home_button_7->setEnabled(true);
        ui->exit_button_7->setEnabled(true);
    }
}


/** Bilder werden richtig skaliert zum Anzeigen in einem Label und werden in ein QPixmap umgewandelt.
**
* @param qimg Input Image
* @param newheight gewünschte Höhe
*/
QPixmap DeepPortrait::scale_image_and_show_in_label(QImage qimg, int newheight)
{
    QPixmap qpix = QPixmap::fromImage(qimg);
    float width = qpix.width();
    float height = qpix.height();
    float ratio = width/height;
    int nh = newheight;
    int nw = ratio*nh;
    qpix=qpix.scaled(nw,nh);
    return qpix;

}



/** Liest Parameter aus dem deepportrait.json File ein
**
*/
void DeepPortrait::read_settings_and_connect_websocket(){
    qDebug() << "readSettingsAndConnectWebsocket";
    QString jsonFilename = "../DeepPortrait/deepportrait.json";
    QFile jsonFile(jsonFilename);

    if(!jsonFile.exists()){
        QMessageBox errorBox(this);
        errorBox.setText("Datei " + jsonFilename + " nicht gefunden !");
        errorBox.setStandardButtons(QMessageBox::Ok);
        errorBox.setDefaultButton(QMessageBox::Ok);

        if(errorBox.exec() == QMessageBox::Ok)
        {
          this->close();
        }
    }
    else{
        jsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
        QString settingsJson = jsonFile.readAll();
        jsonFile.close();

        QJsonDocument jsonDocument = QJsonDocument::fromJson(settingsJson.toUtf8());
        QJsonObject jsonObj        = jsonDocument.object();
        QString settingKey[]       = {"Style_directory" , "Camera_id",
                                      "Smtp_username", "Smtp_password",
                                      "Smtp_server","Smtp_port", "Websocket_url"};

        for(QString key: settingKey){
            QString value = jsonObj.value(key).toString();

            if(value != ""){
                this->settings[key] = QString(value); //Value wird in Hashmap settings geschrieben

                qDebug() << "Json paramter " << key << " : " << value << " gefunden";
            }
            else{
                QMessageBox errorBox;
                errorBox.setText(jsonFilename + " enhält nicht den Key " + key);
                errorBox.setStandardButtons( QMessageBox::Ok);
                errorBox.setDefaultButton(QMessageBox::Ok);
                errorBox.exec();
                close();
            }
        }
    }

    client.set_url(this->settings["Websocket_url"]);
    client.run();

}
