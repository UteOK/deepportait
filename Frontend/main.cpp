/**
 *  ██████╗ ███████╗███████╗██████╗     ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
 *  ██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
 *  ██║  ██║█████╗  █████╗  ██████╔╝    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
 *  ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
 *  ██████╔╝███████╗███████╗██║         ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
 *  ╚═════╝ ╚══════╝╚══════╝╚═╝         ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝
 *
 * VERSION 1.0
 *
 *
 * HDM Stuttgart
 * Medieninformatik
 * Semester Projekt 2018/2019
 *
 * Authorin :  Ute Orner-Klaiber
 * Datum    :  4.2.2019
 * Betreuer :  Benjamin Binder, Patrick Bader
 *
 *
 * @file     main.cpp
 * @author   Ute Orner-Klaiber
 * @brief    TODO
 *
 *
*/

#include "deepportrait.h"
#include <QApplication>
#include <QLabel>
#include <QPixmap>
#include <QDesktopWidget>
#include "Backend/websocket_client.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    DeepPortrait w;
    w.showFullScreen();

    return app.exec();
}
