/*
 *  ██████╗ ███████╗███████╗██████╗     ██████╗  ██████╗ ██████╗ ████████╗██████╗  █████╗ ██╗████████╗
 *  ██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║╚══██╔══╝
 *  ██║  ██║█████╗  █████╗  ██████╔╝    ██████╔╝██║   ██║██████╔╝   ██║   ██████╔╝███████║██║   ██║
 *  ██║  ██║██╔══╝  ██╔══╝  ██╔═══╝     ██╔═══╝ ██║   ██║██╔══██╗   ██║   ██╔══██╗██╔══██║██║   ██║
 *  ██████╔╝███████╗███████╗██║         ██║     ╚██████╔╝██║  ██║   ██║   ██║  ██║██║  ██║██║   ██║
 *  ╚═════╝ ╚══════╝╚══════╝╚═╝         ╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝   ╚═╝
 *
 * VERSION 1.0
 *
 * HDM Stuttgart
 * Medieninformatik
 * Semester Projekt 2018/2019
 *
 * Authorin :  Ute Orner-Klaiber
 *
 * Datum    :  4.2.2019
 *
 * Betreuer :  Benjamin Binder, Patrick Bader
 *
*/

#ifndef DEEPPORTRAIT_H
#define DEEPPORTRAIT_H

#include <QMainWindow>
#include <QTimer>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <QLabel>
#include <QJsonDocument>
#include <QJsonObject>
#include <map>
#include <QPushButton>
#include <QObject>
#include <QSignalMapper>
#include "Backend/websocket_client.h"
#include "Backend/smtp.h"



#define STARTPAGE_INDEX 0
#define MAKE_YOUR_OWN_INDEX 1
#define MAKE_A_FOTO_INDEX 2
#define SHOW_FOTO_INDEX 3
#define CHOOSE_STYLE_INDEX 4
#define USE_AI_INDEX 5
#define SHOW_PROGRESS_INDEX 6
#define SEND_EMAIL_INDEX 7
#define THANK_YOU_INDEX 8

#define INTERN_WEBCAM 0
#define EXTERN_WEBCAM 1

using namespace cv;

namespace Ui {
class DeepPortrait;
}

class DeepPortrait : public QMainWindow
{
    Q_OBJECT
    QTimer timer;
    QTimer timerprogress;


public:
    explicit DeepPortrait(QWidget *parent = nullptr);
    ~DeepPortrait();

signals:
    void next_generated_image();

private slots:

    void fotostream();

    void send_generated_image_request();

    void show_in_label(QString iter);

    void send_content_style();

    void update_progress_bar();

    void read_settings_and_connect_websocket();

    QPixmap scale_image_and_show_in_label(QImage qimg, int newheight);

    void on_home_button_clicked();

    void on_start_button_clicked();

    void on_make_your_own_button_clicked();

    void on_take_a_photo_button_clicked();

    void on_oh_ugly_button_clicked();

    void on_choose_style_button_clicked();

    void on_use_style_button_clicked();

    void on_use_artificial_intelligence_button_clicked();

    void on_reset_generated_image_request();

    void on_manage_generated_image_button_clicked();

    void on_back_to_beginning_button_clicked();

    void on_send_email_button_clicked();

    void on_back_to_start_button_clicked();

    void on_send_finaly_email_button_clicked();

    void on_one_step_back_button_clicked();

    void on_back_to_enter_email_button_clicked();



private:
    Ui::DeepPortrait *ui;
    QSignalMapper *signalMapper;
    QImage content_image;
    QImage style_image;
    QImage generated_image;
    Mat content_image_mat;
    int progress;

    VideoCapture cam;
    websocket_client client; //Objekt Client vom Typ websocket_client

    std::map<QString, QString> settings;
};

#endif // DEEPPORTRAIT_H
