var classwebsocket__client =
[
    [ "websocket_client", "classwebsocket__client.html#a85debb4ae380fe171923054e317459c1", null ],
    [ "add_data", "classwebsocket__client.html#a45bf1a7e942e343f2600ecaeaf970e20", null ],
    [ "add_data", "classwebsocket__client.html#ad97bd64af6ee03059415c522c1cbe8b9", null ],
    [ "add_data", "classwebsocket__client.html#a626dd3e3e9d972c8dd101c5864e89ef4", null ],
    [ "clear_queues", "classwebsocket__client.html#aad6662f647d107372f358b42f573eaf8", null ],
    [ "closed", "classwebsocket__client.html#ac5327b7bb8f03313298f961d285baebb", null ],
    [ "first_request", "classwebsocket__client.html#ad70aacbf494132063fefdd176e58cfed", null ],
    [ "get_data", "classwebsocket__client.html#a12fc2bf1b75b1700032fc2e06c7c4143", null ],
    [ "next_request", "classwebsocket__client.html#a18219c7b95d3eda20686118218c3dbf2", null ],
    [ "next_request_sl", "classwebsocket__client.html#a02551f0c5a6cc9d89084433160e60957", null ],
    [ "next_response", "classwebsocket__client.html#ac6a424b59b08d0e9edcb933a71ed4d4c", null ],
    [ "req_queue_finished", "classwebsocket__client.html#a8bf3fa279cc650d9b82bf02d2ed9cf69", null ],
    [ "run", "classwebsocket__client.html#a91ee25b9131a6bbdfec8f716541a4dd0", null ],
    [ "set_url", "classwebsocket__client.html#ae37f0489a09ee7c6f1bf848437a3df6d", null ]
];